#include <HardwareSerial.h>

int RX_BT = 10;
int TX_BT = 11;

long BtBaudRate = 38400; // or 9600

HardwareSerial BTSerial(1);

void setup() {
  BTserial.begin(BtBaudRate, SERIAL_8N1, RX_BT, TX_BT); 
  Serial.begin(9600);
  delay(500);
}

void loop() {
  if (BTSerial.available()) {
    Serial.write(BTSerial.read());
  }
  if (Serial.available()) {
    BTSerial.write(Serial.read());
  }
}