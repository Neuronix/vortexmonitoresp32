#ifndef Main_h_INCLUDED
#define Main_h_INCLUDED

#include <WiFi.h>
#include <HardwareSerial.h>
#include <ArduinoOTA.h>

#include <LedControl.h>

#include <shared.h>
#include <settings.h>
#include <bluetooth.h>
#include <web.h>

unsigned int voltageInROM;

NextionText speed(nex, 1, 3, "speed");
NextionText voltage(nex, 1, 5, "volts");
NextionText current(nex, 1, 6, "current");
NextionText ah(nex, 1, 13, "ah");
NextionText ahRegen(nex, 1, 15, "ahRegen");
NextionText tempCont(nex, 1, 20, "controllerTemp");
NextionText tempEngine(nex, 1, 18, "engineTemp");
NextionText distance(nex, 1, 21, "distance");
NextionText odometer(nex, 1, 22, "odometer");
NextionText power(nex, 1, 4, "watts");
NextionText dateLabel(nex, 1, 11, "date");
NextionText timeLabel(nex, 1, 12, "time");

NextionText status(nex, 0, 2, "status");
NextionText version(nex, 0, 3, "version");

NextionText maxA(nex, 2, 5, "maxA");
NextionText maxARegen(nex, 2, 6, "maxAReg");
NextionText maxEngT(nex, 2, 10, "maxEngT");
NextionText maxContT(nex, 2, 12, "maxContT");
NextionText maxSpeed(nex, 2, 8, "maxSpeed");
NextionText wattKm(nex, 2, 14, "wattKm");
NextionText rideTime(nex, 2, 16, "rideTime");

NextionText chargeState(nex, 3, 6, "chargeState");
NextionText chargeCC(nex, 3, 8, "cc");
NextionText chargeCV(nex, 3, 10, "cv");

NextionProgressBar ahBar(nex, 1, 1, "ahBar");

NextionPage splash(nex, 0, 0, "splash");
NextionPage main (nex, 1, 0, "main");
NextionPage actions (nex, 3, 0, "actions");

NextionDualStateButton chargeModeButton(nex, 3, 2, "chargeMode");
NextionDualStateButton resetAhButton(nex, 3, 3, "resetAh");
NextionDualStateButton resetDistanceButton(nex, 3, 4, "resetDistance");

boolean initialized = false;
boolean firstRun = true;
boolean regen = false;
boolean chargeMode = false;
boolean stopSendRequests = false;
boolean spiffsMounted = true;

long DAY_MS = 86400000; // 86400000 milliseconds in a day
long HOUR_MS = 3600000; // 3600000 milliseconds in an hour
long MINUTE_MS = 60000; // 60000 milliseconds in a minute

void checkButtons(void);

void showDate();
void setupScreen(void);
void setupNoDataScreen(void);

void displayPower(int power);
void displaySpeed(int speed);

void displayTFTData();
void displayAhBar();

void displayStatistics();

#endif