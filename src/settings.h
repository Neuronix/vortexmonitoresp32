#ifndef settings_h
#define settings_h

// User defined settings
#define CONTROLLER_AUTOSEND true

// ssid to connect
const static char *ssid = "nix";
const static char *password = "secaccess"; // must be greater than 8 chars

// ssid in access point mode
const static char *AP_ssid = "VortexMonitor";
const static char *AP_password = "vortex123"; // must be greater than 8 chars

const static float BATTERY_CAPACITY = 60; // in Ah
const static float FULL_VOLTAGE = 84; // full charged battery voltage
const static float EMPTY_VOLTAGE = 60; // empty battery voltage
const static byte CHARGER_CURRENT = 15; // for charge via engine, in A (not used at this time)
const static bool LI_ION_BATTERY_TYPE = true; // set to true if your battery is li-ion type
const static double WHEEL_DIAMETER = 545;

// serial speed rate
const static long SerialBaudRate = 115200; 
const static long NexBaudRate = 9600; 
const static long BtBaudRate = 38400; 

// HC-05 bluetooth adapter settings
const static byte RX_BT = 14; // RX port
const static byte TX_BT = 27; // TX port

// LED pins
const static byte LED_DIN = 12;
const static byte LED_CLK = 25;
const static byte LED_CS = 26;

#endif settings_h