#include <bluetooth.h>

byte pindex = 0;
byte data;
byte prevData;
byte packetData[260];

HardwareSerial BTserial(1);

void bluetoothTask(void *pvParameters) {  
    BTserial.begin(BtBaudRate, SERIAL_8N2, RX_BT, TX_BT); // 8N2 is not the default settings (8N1)
    BTserial.setRxBufferSize(1024);

    for(;;) {
        delay(100);

        if(!CONTROLLER_AUTOSEND) {
            // sending TrmRequest (request controller for data)
            BTserial.write(-1);     // 0xff
            BTserial.write(-1);     // 0xff
            BTserial.write(1);      // data length
            BTserial.write(113);    // command
            BTserial.write(-115);   // packet crc
        }

        while(BTserial.available()) {
            data = BTserial.read();
            
            if(data == 0xFF && prevData == 0xFF) {
                byte packetLength = BTserial.read();
                byte packetType = BTserial.read();
            
                // data packet (5)
                if(packetType == 5) {
                    memset(packetData, 0, sizeof(packetData));
                    pindex = 0;
                
                    // reading the response of data packet
                    // packet structure is described below code
                    
                    // we are waiting for all data comes to RX
                    while(BTserial.available() < packetLength) {
                        delay(1);
                    }

                    while(pindex < packetLength-1) { // length = type byte + dataBytes[n]
                        packetData[pindex] = BTserial.read();
                        pindex++;
                    }

                    int crc = BTserial.read();
                    int calcCrc = calculateCRC(packetType, packetLength, packetData);

                    if(crc != calcCrc) {
                        continue;
                    }

                    currentData.ImpNa1M = 100.0 / (WHEEL_DIAMETER * 0.314159 / 102.0); // 100 / (diameter * <..> / phases)
                    
                    currentData.speed = packetData[185] + (packetData[186] << 8);
                    if(currentData.speed > 200) {
                        currentData.speed = 0; // FIXME bug with enabled regen
                    }

                    currentData.voltage = ((float)(packetData[38] + (packetData[39] << 8)) / 10);
                    currentData.tempCont = ((float)(packetData[32] + (packetData[33] << 8))) / 10;
                    currentData.tempEngine = ((float)(packetData[34] + (packetData[35] << 8))) / 10;
                    currentData.ah = (float)(((((float)((long)packetData[83]) + (((long)(packetData[84])) << 8) + (((long)(packetData[85])) << 16) + (((long)(packetData[86])) << 24)) / 36000) * 48.34) / 1000);
                    currentData.ahRegen = (float)(((((float)((long)packetData[87]) + (((long)(packetData[88])) << 8) + (((long)(packetData[89])) << 16) + (((long)(packetData[90])) << 24)) / 36000) * 48.34) / 1000);
                    currentData.distance = (float)(round(100 * (((((long)packetData[158]) + (((long)packetData[159]) << 8) + (((long)packetData[160]) << 16) + (((long)packetData[161]) << 24)) / currentData.ImpNa1M) / 1000)) / 100);
                    currentData.odometer = (float)(round(100 * (((((long)packetData[26]) + (((long)packetData[27]) << 8) + (((long)packetData[28]) << 16) + (((long)packetData[29]) << 24)) / currentData.ImpNa1M) / 1000)) / 100);
                    currentData.flgs5 = packetData[95] + (((long)packetData[96]) << 8) + (((long)packetData[97]) << 16) + (((long)packetData[98]) << 24);
                    currentData.unlocked = (currentData.flgs5 & ((long)0x400000)) == 0;

                    if(ahDelta) {
                        currentData.ah = currentData.ah + ahDelta;
                    }

                    if ((((long)currentData.flgs5) & ((long)0x100)) == ((long)0x100)) {
                        currentData.cv = true;
                        currentData.cc = false;
                    } else {
                        currentData.cv = false;
                        currentData.cc = true;
                    }

                    currentData.current = (packetData[20] + (((int8_t)packetData[21]) << 8)) * 0.048;

                    checkMaxData();

                    DateTime now = rtc.now();
                    lastDataSeen = now.unixtime();

                    if(!gotData) {
                      gotData = true;
                    }
                }
            }

            prevData = data;
        }   
    }
}

void checkMaxData(void) {
  if(currentData.speed > maxData.speed) {
    maxData.speed = currentData.speed;
  }
  if(currentData.current > maxData.current) {
    maxData.current = currentData.current;
  }
  if(currentData.current < 0 && currentData.current < maxData.currentRegen) {
    maxData.currentRegen = currentData.current;
  }
  if(currentData.tempEngine > maxData.tempEngine) {
    maxData.tempEngine = currentData.tempEngine;
  }
  if(currentData.tempCont > maxData.tempCont) {
    maxData.tempCont = currentData.tempCont;
  }
}

byte calculateCRC(int type, int plength, byte *data) {
  unsigned int summ = type + plength;
  
  for (byte j = 0; j < (plength-1); j++) {
    summ = summ + data[j];
  }
  summ = ~summ;
  
  return (byte)summ;
}

void sendUnlock(void) {
  if(!currentData.unlocked) {
    currentData.unlocked = true;
    // send SEND_unlock command for unlock controller
    BTserial.write(-1);      // 0xff
    BTserial.write(-1);      // 0xff
    BTserial.write(13);      // data length
    BTserial.write(243);     // command
    BTserial.write(0x37);    // data
    BTserial.write(0xac);    // data
    BTserial.write(0x2b);    // data
    BTserial.write(0x33);    // data
    BTserial.write(0xf1);    // data
    BTserial.write(0x91);    // data
    BTserial.write(0x7a);    // data
    BTserial.write(0xb0);    // data
    BTserial.write(0xec);    // data
    BTserial.write(0x46);    // data
    BTserial.write(0x10);    // data
    BTserial.write(0xaa);    // data
    BTserial.write(38);      // packet crc 
  }
}

void sendLock(void) {
  if(currentData.unlocked) {
    currentData.unlocked = false;
    // send SEND_lock command for lock again
    BTserial.write(-1);     // 0xff
    BTserial.write(-1);     // 0xff
    BTserial.write(1);      // data length
    BTserial.write(245);    // command
    BTserial.write(9);      // packet crc  
  }
}

void sendSaveSettings(void) {
  // sending SEND_ProgrammOptions
  BTserial.write(-1);     // 0xff
  BTserial.write(-1);     // 0xff
  BTserial.write(1);      // data length
  BTserial.write(15);     // command
  BTserial.write(-17);    // packet crc
}

void sendResetTrip(void) {  
  // sending SEND_ResetDistance
  BTserial.write(-1);     // 0xff
  BTserial.write(-1);     // 0xff
  BTserial.write(1);      // data length
  BTserial.write(184);    // command
  BTserial.write(70);     // packet crc
}

void sendResetAh(void) {
  // send SEND_ClearCurrentAH command
  BTserial.write(-1);     // 0xff
  BTserial.write(-1);     // 0xff
  BTserial.write(1);      // data length
  BTserial.write(108);    // command
  BTserial.write(-110);   // packet crc
}

void sendChargerCurr(void) {
  int chargeA = (int)((CHARGER_CURRENT * 1000) / 48.34);
  byte trm[4];

  // send SEND_ChagerCurr command
  trm[0] = 71;
  trm[1] = (byte)chargeA;
  trm[2] = (byte)(chargeA >> 8); 
  trm[3] = calculateCRC(254, 4, trm);

  BTserial.write(-1);   
  BTserial.write(-1);   
  BTserial.write(4);     
  BTserial.write(254);

  for(byte i = 0; i <= 3; i++) {
    BTserial.write(trm[i]);
  }
}

void sendStartCharge(void) {
  sendUnlock();

  delay(2000);

  // send SEND_ChagerViaMotorOn command
  BTserial.write(-1);     // 0xff
  BTserial.write(-1);     // 0xff
  BTserial.write(2);      // data length
  BTserial.write(254);    // NEW_CMD byte
  BTserial.write(66);     // SEND_ChagerViaMotorOn
  BTserial.write(-67);    // packet crc'

  delay(1500);

  //sendChargerCurr();

  //delay(1500);

  sendLock();
}

void sendStopCharge(void) {
  sendUnlock();

  delay(2000);

  // send SEND_ChagerViaMotorOff command
  BTserial.write(-1);     // 0xff
  BTserial.write(-1);     // 0xff
  BTserial.write(2);      // data length
  BTserial.write(254);    // NEW_CMD byte
  BTserial.write(67);     // SEND_ChagerViaMotorOff
  BTserial.write(-68);    // packet crc

  delay(1500);

  sendLock();
}

/* 
 *  -----------------------------------------------
 *  Data (5) packet structure
 *  -----------------------------------------------
 *  
 *  packetData[0] // CntSample
 *  packetData[1] // CntSample
 *  packetData[2] // CntSample
 *  packetData[3] // CntSample
 *  
 *  packetData[4] // AlfaXRes
 *  packetData[5] // AlfaXRes
 *  
 *  packetData[6] // AlfaYRes
 *  packetData[7] // AlfaYRes
 *  
 *  packetData[8] // GyroX
 *  packetData[9] // GyroX
 *  
 *  packetData[10] // TiltXres
 *  packetData[11] // TiltXres
 *  
 *  packetData[12] // TiltYres
 *  packetData[13] // TiltYres
 *  
 *  packetData[14] // Ep
 *  packetData[15] // Ep
 *  
 *  packetData[16] // Ei
 *  packetData[17] // Ei
 *  
 *  packetData[18] // PWM1
 *  packetData[19] // PWM1
 *  
 *  packetData[20] // Curr1 --- battery current
 *  packetData[21] // Curr1 --- battery current
 *  
 *  packetData[22] // Spd1USTKMH
 *  packetData[23] // Spd1USTKMH
 *  
 *  packetData[24] // Temperature3
 *  packetData[25] // Temperature3
 *  
 *  packetData[26] // Odometer
 *  packetData[27] // Odometer
 *  packetData[28] // Odometer
 *  packetData[29] // Odometer
 *  
 *  packetData[30] // Spd1Res
 *  packetData[31] // Spd1Res
 *  
 *  packetData[32] // Temperature2 --- controller temp
 *  packetData[33] // Temperature2 --- controller temp
 *  
 *  packetData[34] // Temperature1 --- engine temp
 *  packetData[35] // Temperature1 --- engine temp
 *  
 *  packetData[36] // Kp
 *  packetData[37] // Kp
 *  
 *  packetData[38] // UBT ------ BATTERY
 *  packetData[39] // UBT ------ VOLTAGE
 *  
 *  packetData[40] // GyroVert_Steer
 *  packetData[41] // GyroVert_Steer
 *  
 *  packetData[42] // StatFlags
 *  packetData[43] // StatFlags
 *  
 *  packetData[44] // GyroZFilter
 *  packetData[45] // GyroZFilter
 *  
 *  packetData[46] // AlfaYResPrevAv
 *  packetData[47] // AlfaYResPrevAv
 *  
 *  packetData[48] // DiffBetweenTilts
 *  packetData[49] // DiffBetweenTilts
 *  
 *  packetData[50] // Flgs
 *  packetData[51] // Flgs
 *  packetData[52] // Flgs
 *  packetData[53] // Flgs
 *  
 *  packetData[54] // Flgs1
 *  packetData[55] // Flgs1
 *  packetData[56] // Flgs1
 *  packetData[57] // Flgs1
 *  
 *  packetData[58] // Flgs2
 *  packetData[59] // Flgs2
 *  packetData[60] // Flgs2
 *  packetData[61] // Flgs2
 *  
 *  packetData[62] // TiltZad
 *  packetData[63] // TiltZad
 *  
 *  packetData[64] // StartRot
 *  packetData[65] // StartRot
 *  
 *  packetData[66] // Ki
 *  packetData[67] // Ki
 *  
 *  packetData[68] // KRot
 *  packetData[69] // KRot
 *  
 *  packetData[70] // KpRot
 *  packetData[71] // KpRot
 *  
 *  packetData[72] // KiRot
 *  packetData[73] // KiRot
 *  
 *  packetData[74] // Preas1ADC
 *  packetData[75] // Preas1ADC
 *  
 *  packetData[76] // Preas2ADC
 *  packetData[77] // Preas2ADC
 *  
 *  packetData[78] // CurrLimitTek
 *  
 *  packetData[79] // KpSPD
 *  packetData[80] // KpSPD
 *  
 *  packetData[81] // AngleLimitReal
 *  packetData[82] // AngleLimitReal
 *  
 *  packetData[83] // CurrTuda1 ------------
 *  packetData[84] // CurrTuda1 ------------  Ah consumed
 *  packetData[85] // CurrTuda1 ------------
 *  packetData[86] // CurrTuda1 ------------
 *  
 *  packetData[87] // CurrRegen1 ------------
 *  packetData[88] // CurrRegen1 ------------ Ah regen
 *  packetData[89] // CurrRegen1 ------------
 *  packetData[90] // CurrRegen1 ------------
 *  
 *  packetData[91] // Flgs4
 *  packetData[92] // Flgs4
 *  packetData[93] // Flgs4
 *  packetData[94] // Flgs4
 *  
 *  packetData[95] // Flgs5
 *  packetData[96] // Flgs5
 *  packetData[97] // Flgs5
 *  packetData[98] // Flgs5
 *  
 *  packetData[99] // Flgs3
 *  packetData[100] // Flgs3
 *  packetData[101] // Flgs3
 *  packetData[102] // Flgs3
 *  
 *  packetData[103] // TimerCnt
 *  packetData[104] // TimerCnt
 *  packetData[105] // TimerCnt
 *  packetData[106] // TimerCnt
 *  
 *  packetData[107] // _3VFl
 *  packetData[108] // _3VFl
 *  
 *  packetData[109] // _5VFl
 *  packetData[110] // _5VFl
 *  
 *  packetData[111] // _12VFl
 *  packetData[112] // _12VFl
 *  
 *  packetData[113] // V4
 *  packetData[114] // V4
 *  
 *  packetData[115] // V5
 *  packetData[116] // V5
 *  
 *  packetData[117] // V6
 *  packetData[118] // V6
 *  
 *  packetData[119] // V7
 *  packetData[120] // V7
 *  
 *  packetData[121] // V8
 *  packetData[122] // V8
 *  
 *  packetData[123] // V9
 *  packetData[124] // V9
 *  
 *  packetData[125] // V10
 *  packetData[126] // V10
 *  
 *  packetData[127] // V11
 *  packetData[128] // V11 
 *  
 *  packetData[129] // V12
 *  packetData[130] // V12
 *  
 *  packetData[131] // V13
 *  packetData[132] // V13
 *  
 *  packetData[133] // V14
 *  packetData[134] // V14
 *  
 *  packetData[135] // V15
 *  packetData[136] // V15
 *  
 *  packetData[137] // V16
 *  packetData[138] // V16
 *  
 *  packetData[139] // V17
 *  packetData[140] // V17
 *  
 *  packetData[141] // V18
 *  packetData[142] // V18
 *  
 *  packetData[143] // BatteryKeys
 *  packetData[144] // BatteryKeys
 *  packetData[145] // BatteryKeys
 *  packetData[146] // BatteryKeys
 *  
 *  packetData[147] // ChagerKeys
 *  
 *  packetData[148] // AccX
 *  packetData[149] // AccX
 *  
 *  packetData[150] // AccY
 *  packetData[151] // AccY
 *  
 *  packetData[152] // AccZ
 *  packetData[153] // AccZ
 *  
 *  packetData[154] // Sensor1_Prev
 *  
 *  packetData[155] // Sensor2_Prev
 *  
 *  packetData[156] // Temperature4
 *  packetData[157] // Temperature4
 *  
 *  packetData[158] // Distance
 *  packetData[159] // Distance
 *  packetData[160] // Distance
 *  packetData[161] // Distance
 *  
 *  packetData[162] // ProtectValue
 *  packetData[163] // ProtectValue
 *  
 *  packetData[164] // Temperature
 *  
 *  packetData[165] // _48V
 *  
 *  packetData[166] // KAccGyroSteer
 *  
 *  packetData[167] // GyroXFl
 *  packetData[168] // GyroXFl
 *  
 *  packetData[169] // GyroYFl
 *  packetData[170] // GyroYFl
 *  
 *  packetData[171] // GyroZFl
 *  packetData[172] // GyroZFl
 *  
 *  packetData[173] // Ed
 *  packetData[174] // Ed
 *  
 *  packetData[175] // GyroYAv
 *  packetData[176] // GyroYAv
 *  
 *  packetData[177] // KdI
 *  packetData[178] // KdI
 *  
 *  packetData[179] // MaxSpdKMH
 *  packetData[180] // MaxSpdKMH
 *  
 *  packetData[181] // PhasesCnt2
 *  packetData[182] // PhasesCnt2
 *  packetData[183] // PhasesCnt2
 *  packetData[184] // PhasesCnt2
 *  
 *  packetData[185] // Spd1Fl --\ SPEED
 *  packetData[186] // Spd1Fl --/ SPEED
 *  
 *  packetData[187] // Temperature5
 *  packetData[188] // Temperature5 
 *  
 *  packetData[189] // Temperature6
 *  packetData[190] // Temperature6 
 *  
 *  packetData[191] // Phase1Period1
 *  packetData[192] // Phase1Period1 
 *  packetData[193] // Phase1Period1
 *  packetData[194] // Phase1Period1 
 *  
 *  packetData[195] // Phase1Period2
 *  packetData[196] // Phase1Period2 
 *  packetData[197] // Phase1Period2
 *  packetData[198] // Phase1Period2 
 *
 *  packetData[199] // EpCurr1
 *  packetData[200] // EpCurr1 
 *  packetData[201] // EpCurr1
 *  packetData[202] // EpCurr1 
 *  
 *  packetData[203] // Phase1Period4
 *  packetData[204] // Phase1Period4 
 *  packetData[205] // Phase1Period4
 *  packetData[206] // Phase1Period4 
 *  
 *  packetData[207] // Phase1Period5
 *  packetData[208] // Phase1Period5 
 *  packetData[209] // Phase1Period5
 *  packetData[210] // Phase1Period5 
 *  
 *  packetData[211] // Phase1Period6
 *  packetData[212] // Phase1Period6  
 *  packetData[213] // Phase1Period6
 *  packetData[214] // Phase1Period6 
 *  
 *  packetData[215] // PDC3
 *  packetData[216] // PDC3
 *  
 *  packetData[217] // CriticalError
 *  
 *  packetData[218] // Preas1ADCResCalibr
 *  packetData[219] // Preas1ADCResCalibr 
 *  
 *  packetData[220] // ADCThrottleBreak
 *  packetData[221] // ADCThrottleBreak 
 *  
 *  packetData[222] // RCON_Mem
 *  packetData[223] // RCON_Mem 
 *  
 *  packetData[224] // FirmwareVersion
 *  packetData[225] // FirmwareVersion 
 *  
 *  packetData[226] // MPU6050Err
 *  
 *  packetData[227] // PhaseCurr
 *  packetData[228] // PhaseCurr 
 *  
 *  packetData[229] // HallErrCnt
 *  packetData[230] // HallErrCnt 
 *  
 *  packetData[231] // CurrPhaseLimitTek
 *  packetData[232] // CurrPhaseLimitTek
 *  
 *  packetData[233] // PhasePer1
 *  packetData[234] // PhasePer1 
 *  packetData[235] // PhasePer1
 *  packetData[236] // PhasePer1 
 *  
 *  packetData[237] // Halls
 *  
 *  packetData[238] // HallDelay1_Tek_F
 *  
 *  packetData[239] // HallDelay1_Tek_B 
 */